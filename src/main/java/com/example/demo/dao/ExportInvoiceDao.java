package com.example.demo.dao;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.util.ResourceUtils;

import com.example.demo.dto.ExportInvoiceDto;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;


public class ExportInvoiceDao {
	public static void exportInvoiceToPdf(List<ExportInvoiceDto> exportInvoiceDtos, HttpServletResponse response) throws IOException, JRException {
        File file = ResourceUtils.getFile("classpath:Invoice.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(exportInvoiceDtos);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }


}
