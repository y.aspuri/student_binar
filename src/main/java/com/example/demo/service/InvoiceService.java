package com.example.demo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ExportInvoiceDao;
import com.example.demo.dto.ExportInvoiceDto;
import com.example.demo.dto.GetInvoiceDto;
import com.example.demo.dto.GetScheduleDto;
import com.example.demo.dto.PostInvoiceDto;
import com.example.demo.entity.Film;
import com.example.demo.entity.Invoice;
import com.example.demo.entity.Schedule;
import com.example.demo.repository.InvoiceRepository;

import net.sf.jasperreports.engine.JRException;

@Service
public class InvoiceService {
	@Autowired
	private InvoiceRepository invoiceRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ScheduleService scheduleService;
	
	@Autowired
	private FilmService filmService;

	
	
	public Invoice save(PostInvoiceDto postInvoiceDto) {
		Invoice invoice = new Invoice();
		Optional<Schedule> schedule = scheduleService.getScheduleById(postInvoiceDto.getScheduleId());
		if (schedule.isEmpty()) {
			throw new RuntimeException("Schedule not found");
		}
		invoice.setUser(userService.findUserById(postInvoiceDto.getUserId()));
		invoice.setSchedule(schedule.get());
		invoice.setSeatNumber(postInvoiceDto.getSeatNumber());
		return invoiceRepository.save(invoice);
	}
		
		
	public List<GetInvoiceDto> findAll() {
		List<Invoice> invoices = invoiceRepository.findAll();
        List<GetInvoiceDto> getInvoiceDtos = new ArrayList<>();
        for (Invoice invoice : invoices) {
            Schedule schedule = scheduleService.getScheduleById(invoice.getSchedule().getId()).isPresent() ? scheduleService.getScheduleById(invoice.getSchedule().getId()).get() : null;
            Film film = filmService.findFilmById(schedule.getFilmCode()).get();
            GetScheduleDto getScheduleDto = GetScheduleDto.build(schedule, film);
            getInvoiceDtos.add(GetInvoiceDto.toDto(invoice, getScheduleDto));invoiceRepository.findAll();
        }
        return getInvoiceDtos;
    }

    public GetInvoiceDto findById(Long id) {
        Invoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new RuntimeException("Invoice not found");
        }
        Schedule schedule = scheduleService.getScheduleById(invoice.getSchedule().getId()).get();
        Film film = filmService.findFilmById(schedule.getFilmCode()).get();
        GetScheduleDto getScheduleDto = GetScheduleDto.build(schedule, film);
        return GetInvoiceDto.toDto(invoice, getScheduleDto);
    }

    public void exportAllInvoices(HttpServletResponse response) throws JRException, IOException {
        List<ExportInvoiceDto> exportInvoiceDtos = new ArrayList<>();
        List<Invoice> invoices = invoiceRepository.findAll();
        for (Invoice invoice : invoices) {
            Schedule schedule = scheduleService.getScheduleById(invoice.getSchedule().getId()).get();
            Film film = filmService.findFilmById(schedule.getFilmCode()).get();
            ExportInvoiceDto exportInvoiceDto = new ExportInvoiceDto(
                    invoice.getUser().getUsername(),
                    film.getTitle(),
                    schedule.getDate(),
                    schedule.getStartTime() + " - " + schedule.getEndTime(),
                    invoice.getSeatNumber()
            );
            exportInvoiceDtos.add(exportInvoiceDto);
        }
        response.setHeader("Content-Disposition", "attachment; filename=invoice.pdf");
        ExportInvoiceDao.exportInvoiceToPdf(exportInvoiceDtos, response);
    }

    public void exportInvoiceById(Long id, HttpServletResponse response) throws IOException, JRException {
        Invoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new RuntimeException("Invoice not found");
        }
        List<ExportInvoiceDto> exportInvoiceDtos = new ArrayList<>();
        String filmTitle = filmService.findFilmById(invoice.getSchedule().getFilmCode()).get().getTitle();
        exportInvoiceDtos.add(
                new ExportInvoiceDto(
                        invoice.getUser().getUsername(),
                        filmTitle,
                        invoice.getSchedule().getDate(),
                        invoice.getSchedule().getStartTime() + " - " + invoice.getSchedule().getEndTime(),
                        invoice.getSeatNumber()
                )
        );

        String fileName = "invoice_" + invoice.getUser().getUsername() + "_" + invoice.getId() + ".pdf";
        response.setHeader("Content-Disposition", "inline; filename=" + fileName);

        ExportInvoiceDao.exportInvoiceToPdf(exportInvoiceDtos, response);
    }

    public void deleteById(Long id) {
        invoiceRepository.deleteById(id);
    }

    public Invoice update(Long id, PostInvoiceDto postInvoiceDto) {
        Invoice invoiceToUpdate = invoiceRepository.getById(id);
        if (invoiceToUpdate == null) {
            return null;
        }
		invoiceToUpdate.setUser(userService.findUserById(postInvoiceDto.getUserId()));
        invoiceToUpdate.setSchedule(scheduleService.getScheduleById(postInvoiceDto.getScheduleId()).get());
        invoiceToUpdate.setSeatNumber(postInvoiceDto.getSeatNumber());
        return invoiceRepository.save(invoiceToUpdate);
    }

}
