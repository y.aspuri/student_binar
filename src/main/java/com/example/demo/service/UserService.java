package com.example.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.PostUserDto;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;


@Service
public class UserService {
	
	
	
	@Autowired
	private UserRepository userRepository;
	
	public User save(User user) {
		return userRepository.save(user);
	}
	
	public User signUp(User body) {
		return userRepository.save(body);
	}
	
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	public Iterable<User> findAllUsers() {
		return userRepository.findAll();
	}
	
	public User findUserById(Long id) {
		return userRepository.findById(id).isPresent()
				? userRepository.findById(id).get()
						: null;
	}
	
	public String deleteUserById(Long id) {
		Optional<User> user = userRepository.findById(id);
		if (user.isEmpty()) {
			return "User not found" ;
		}
		userRepository.deleteById(id);
		return " User dengan Id" + id + "telah dihapus" ;
	}
	
	public User updateUser(long id, PostUserDto postUserDto) {
		User user = findUserById(id);
		if (user == null) {
			throw new IllegalArgumentException("User not found");
		}
		user.setUsername(postUserDto.getUsername());
		user.setPassword(postUserDto.getPassword());
		user.setEmail(postUserDto.getEmail());
		return userRepository.save(user);
	}



}
