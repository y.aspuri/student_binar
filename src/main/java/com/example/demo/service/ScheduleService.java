package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.GetScheduleDto;
import com.example.demo.entity.Film;
import com.example.demo.entity.Schedule;
import com.example.demo.repository.ScheduleRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ScheduleService {
    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private FilmService filmService;


    public List<GetScheduleDto> getAllSchedule() {
        List<Schedule> Schedules = scheduleRepository.findAll();
        List<GetScheduleDto> getScheduleDtos = new ArrayList<>();
        for (Schedule schedule : Schedules) {
        	Film film = filmService.findFilmById(schedule.getFilmCode()).get();
            GetScheduleDto getScheduleDto = GetScheduleDto.build(schedule, film);
            getScheduleDtos.add(getScheduleDto);
        }
        return getScheduleDtos;
    }

    public Optional<Schedule> getScheduleById(Long id) {
        return scheduleRepository.findById(id);
    }

    public Optional<GetScheduleDto> getScheduleDtoById(Long id) {
        Optional<Schedule> reqSchedule = scheduleRepository.findById(id);
        if (reqSchedule.isEmpty()) {
            throw new IllegalArgumentException("Jadwal tidak ditemukan");
        }
        Schedule schedule = reqSchedule.orElse(null);
        Film film = filmService.findFilmById(schedule.getFilmCode()).isPresent()
                ? filmService.findFilmById(schedule.getFilmCode()).get()
                : null;
        if (film == null) {
            throw new IllegalArgumentException("Film tidak ditemukan");
        }
        GetScheduleDto getScheduleDto = new GetScheduleDto(
                schedule.getId(),
                schedule.getDate(),
                schedule.getPrice(),
                schedule.getStartTime(),
                schedule.getEndTime(),
                new GetScheduleDto.ScheduleFilm(schedule.getFilmCode(), film.getTitle())
        );
        return Optional.of(getScheduleDto);
    }

    public Schedule saveSchedule(Schedule schedule) {
        Optional<Film> film = filmService.findFilmById(schedule.getFilmCode());
        if (film.isEmpty()) {
            throw new IllegalArgumentException("Film not found");
        }
        return scheduleRepository.save(schedule);
    }

    public String deleteSchedule(Long id) {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        if (schedule.isEmpty()) {
            return "Jadwal tidak ditemukan";
        }
        scheduleRepository.deleteById(id);
        return "Jadwal dengan ID " + id + " berhasil dihapus";
    }
}
