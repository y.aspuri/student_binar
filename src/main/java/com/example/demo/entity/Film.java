package com.example.demo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.demo.dto.PostFilmDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "film")
public class Film {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column(name = "code", nullable = false)
	private Long code;
	
	@Column(name= " title", nullable = false)
	private String title;
	
	@Column(name = "Showing", nullable = false)
	private boolean Showing;
	
	@OneToMany(targetEntity = Schedule.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "film_code", referencedColumnName = "code")
	private List<Schedule> schedules;
	
	public PostFilmDto toDto() {
		return new PostFilmDto();
	}

}
