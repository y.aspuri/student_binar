package com.example.demo.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.dto.PostUserDto;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;
    
//    @PostMapping("/signup")
//    public ResponseEntity<User>Post(@RequestBody User body) {
//    	URI uri =URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/user").toUriString());
//    	return ResponseEntity.created(uri).body(userService.signUp(body));
//    }

    @GetMapping("/")
    public Iterable<User> getAllUser() {
        return userService.findAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) {
        return userService.findUserById(id);
    }

    @PostMapping("/")
    public User addUser(@RequestBody User user) {
        return userService.save(user);
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable Long id, @RequestBody PostUserDto postUserDto) {
        return userService.updateUser(id, postUserDto);
    }

    @DeleteMapping("/{id}")
    public String deleteUser(@PathVariable Long id) {
        return userService.deleteUserById(id);
    }
}

