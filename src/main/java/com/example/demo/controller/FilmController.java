package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PostFilmDto;
import com.example.demo.entity.Film;
import com.example.demo.entity.Schedule;
import com.example.demo.service.FilmService;

@RestController
@RequestMapping("/api/film")
public class FilmController {
    @Autowired
    private FilmService filmService;

    @GetMapping("/")
    public Iterable<Film> getAllFilms() {
        return filmService.findAllFilm();
    }

    @GetMapping("/is_showing")
    public Iterable<Film> getFilmsShowing() {
        return filmService.findAllFilm(true);
    }

    @GetMapping("/{id}")
    public Optional<Film> getFilmById(@PathVariable("id") Long id) {
        return filmService.findFilmById(id);
    }

    @GetMapping("/{id}/schedule")
    public Iterable<Schedule> getScheduleByFilmId(@PathVariable("id") Long id) {
        return filmService.findScheduleByFilmId(id);
    }

    @PostMapping("/")
    public Film addFilm(@RequestBody PostFilmDto postFilmDto) {
        return filmService.saveFilm(postFilmDto);
    }

    @PutMapping("/{id}/schedule")
    public Film addSchedule(
            @PathVariable("id") Long id,
            @RequestBody Schedule schedule
    ) {
        return filmService.addSchedule(id, schedule);
    }

    @PutMapping("/{code}/schedule/{scheduleId}")
    public Film removeSchedule(
            @PathVariable("code") Long id,
            @PathVariable("scheduleId") Long scheduleId
    ) {
        return filmService.deleteSchedule(id, scheduleId);
    }

    @PutMapping("/{code}")
    public Film updateTitle(@PathVariable long code, @RequestParam String title) {
        return filmService.updateFilmTitle(code, title);
    }

    @DeleteMapping("/{id}")
    public String deleteFilm(@PathVariable("id") Long id) {
        return filmService.deleteFilmById(id);
    }

}
