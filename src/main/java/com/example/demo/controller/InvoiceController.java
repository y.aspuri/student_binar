package com.example.demo.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.GetInvoiceDto;
import com.example.demo.dto.PostInvoiceDto;
import com.example.demo.entity.Invoice;
import com.example.demo.service.InvoiceService;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/")
    public List<GetInvoiceDto> getAllInvoices() {
        return invoiceService.findAll();
    }
    
    @GetMapping("/export")
    public void exportAllInvoices(HttpServletResponse response) throws JRException, IOException {
    	invoiceService.exportAllInvoices(response);
    }

    @GetMapping("/{id}")
    public GetInvoiceDto getInvoiceById(@PathVariable Long id) {
        return invoiceService.findById(id);
    }

    @GetMapping("/{id}/export")
    public void exportInvoice(@PathVariable Long id, HttpServletResponse response) throws JRException, IOException {
        invoiceService.exportInvoiceById(id, response);
    }

    @PostMapping("/")
    public Invoice createInvoice(PostInvoiceDto postInvoiceDto) {
        return invoiceService.save(postInvoiceDto);
    }

    @PutMapping("/{id}")
    public Invoice updateInvoice(Long id, PostInvoiceDto postInvoiceDto) {
        return invoiceService.update(id, postInvoiceDto);
    }

    @DeleteMapping("/{id}")
    public void deleteInvoice(Long id) {
        invoiceService.deleteById(id);
    }

}
