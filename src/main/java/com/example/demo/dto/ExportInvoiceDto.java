package com.example.demo.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class ExportInvoiceDto implements Serializable {
    private final String username;
    private final String film_title;
    private final String date;
    private final String time;
    private final String seat_number;
}
