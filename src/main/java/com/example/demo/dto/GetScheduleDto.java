package com.example.demo.dto;

import java.io.File;
import java.io.Serializable;

import com.example.demo.entity.Film;
import com.example.demo.entity.Schedule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetScheduleDto implements Serializable {
	private Long id;
	private String date;
	private Long price;
	private String startTime;
	private String endTime;
	private ScheduleFilm film;
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ScheduleFilm {
		private Long code;
		private String title;
	}
	
	public static GetScheduleDto build(Schedule schedule, Film film) {
		return new GetScheduleDto(
				schedule.getId(),
				schedule.getDate(),
				schedule.getPrice(),
				schedule.getStartTime(),
				schedule.getEndTime(),
				new ScheduleFilm(film.getCode(), film.getTitle()));
	}
	
}
