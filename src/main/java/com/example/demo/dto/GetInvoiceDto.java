package com.example.demo.dto;

import java.io.Serializable;

import com.example.demo.entity.Invoice;
import com.example.demo.entity.User;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GetInvoiceDto implements Serializable {
	private Long id;
	private User user;
	private GetScheduleDto schedule;
	private String seatNumber;
	
	public static GetInvoiceDto toDto(Invoice invoice, GetScheduleDto schedule) {
		return new GetInvoiceDto(invoice.getId(), (User) invoice.getUser(), schedule, invoice.getSeatNumber());
	}

}
