package com.example.demo.dto;

import java.io.Serializable;
import java.util.List;

import com.example.demo.entity.Schedule;

import lombok.Data;

@Data
public class PostFilmDto implements Serializable {
	private  String title;
	private boolean Showing;
	private List<Schedule> schedules;
	}


