package com.example.demo.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PostInvoiceDto implements Serializable {
	private final Long userId;
	private final Long scheduleId;
	private final String seatNumber;

}
